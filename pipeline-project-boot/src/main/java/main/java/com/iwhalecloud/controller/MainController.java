package main.java.com.iwhalecloud.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * *
 *
 * @author tanye.tyn
 * @date 2019/7/23
 */
@RestController
@RequestMapping("api")
public class MainController {

    @RequestMapping(value = "/checkHealth", method = RequestMethod.GET)
    public String checkHealth() {
        return "V1:健康";
    }

}
